# **XalapaCode vs Coronavirus**

El propósito de este documento es concretar algunas ideas de acción contra el COVID-19 en Xalapa y sus alrededores desde el punto de vista de la tecnología. Está organizado por las siguientes secciones:

1. [*Contexto*](https://gitlab.com/arturomf94/covid19xalapa#1-contexto) - descripción del problema y puntos relevantes para la acción comunitaria.
2. [*Ejemplos*](https://gitlab.com/arturomf94/covid19xalapa#2-ejemplos) - lista de ejemplos concretos sobre acciones que se han llevado a cabo en otros lugares del mundo **y** que han sido efectivas o tienen razón para ser efectivas en Xalapa también.
3. [*Ideas*](https://gitlab.com/arturomf94/covid19xalapa#3-ideas) - Ideas de acciones que podríamos tomar o herramientas que podríamos construir dados los ejemplos de la sección anterior y el contexto, tanto global como local, para que estas ideas puedan ser efectivas.
4. [*Material Extra*](https://gitlab.com/arturomf94/covid19xalapa#4-material-extra) - lista de enlaces adicionales sobre el tema.

Si quieres contribuir en este documento puedes editar cualquiera de las secciones. Abre un PR para discutir la edición y la idea en concreto.

## 1. Contexto

Para tener una idea completa sobre el contexto de este problema es buena idea leer [la traducción](https://medium.com/@tomaspueyo/coronavirus-por-qu%C3%A9-debemos-actuar-ya-93079c61e200) de [este artículo](https://medium.com/@tomaspueyo/coronavirus-act-today-or-people-will-die-f4d3d9cd99ca). Por ahora, dejamos aquí las ideas más relevantes para este documento:

* El índice de creciemiento de los casos de COVID-19 es exponencial en muchos países. La mayoría de estos países son occidentales y al día de hoy (15 de marzo 2020), excluyendo a China, que ha logrado ya una contención exitosa, los países más afectados están en Europa (por ejemplo: Italia y España), aunque también hay casos graves en Irán y Corea del Sur.
* Algunos lugares han tenido planes de acción exitosos, como en Taiwán, Hong Kong y Singapur, gracias a la transparencia, pruebas clínicas, cuarentenas y el aislamiento de casos sospechosos.
* En México los datos confirmados al día de hoy (15 de marzo) son los siguientes:

![datos_mexico](https://gitlab.com/arturomf94/covid19xalapa/-/raw/master/docs/mexico_covid19.png)

* De acuerdo con [la información del Gobierno de México](https://www.gob.mx/salud/documentos/nuevo-coronavirus) las [recomendaciones para la población](https://www.gob.mx/salud/documentos/nuevo-coronavirus-poblacion) son:

  1. Lavarse las manos frecuentemente.

  2. Utilizar el estornudo de etiqueta.

  3. No escupir.

  4. No tocarse la cara con las manos sucias.

  5. Limpiar y desinfectar superficies de uso común.

  6. **Quedarse en casa cuando se tienen enfermedades respiratorias** y acudir al médico si se presenta alguno de los síntomas (fiebre mayor a 38° C, dolor de cabeza, dolor de garganta, escurrimiento nasal, etc.).

  7. **Evitar** contacto con gente que tenga enfermedades respiratorias.



  Las recomendaciones para el personal de salud son más extensas y pueden consultarse [aquí](https://www.gob.mx/salud/documentos/informacion-para-personal-de-salud). La información más relevante para este documento es la siguiente:

  ![gob_info_salud](https://gitlab.com/arturomf94/covid19xalapa/-/raw/master/docs/gob_info_salud.png)

* Ver [plan de acción en Veracruz](https://gitlab.com/arturomf94/covid19xalapa/-/raw/master/docs/plan_covid10_ver.pdf?inline=false).

## 2. Ejemplos

1. [Mapas en tiempo real en Taiwán](https://mask.pdis.nat.gov.tw/?fbclid=IwAR0K7R4_14ztQ1bEY0UiQmwsfoA9e3iQhttowEkkMej647aOhIo_RfxvggA)
2. [Sistema de notificaciones (WhatsApp)](https://govinsider.asia/innovation/singapore-coronavirus-whatsapp-covid19-open-government-products-govtech/)

## 3. Ideas

Lo más adecuado sería atacar el problema desde cualquiera de las siguientes dos perspectivas:

1. **Compartiendo información**.
2. **Combatiendo desinformación**.

Para el punto 1 podríamos recolectar información sobre lugares con abastecimiento de ciertos productos, como cubrebocas, medicinas, etc. Otra opción es compartir noticias por parte de las autoridades de salud de la región.

Para el punto 2 podríamos hacer una coordinación con alguna autoridad para establecer una comunicación directa con alguien que pueda desmentir ciertas recomendaciones falsas en Xalapa.

Una consideración es que [Apple y Google tienen restricciones](https://www.cnbc.com/2020/03/05/apple-rejects-coronavirus-apps-that-arent-from-health-organizations.html) sobre el tipo de aplicaciones que pueden descargarse de la App Store de Apple y Google Play. De tal manera, quizá lo más adecuado en esta tarea sería llevar a cabo el proyecto en una página web.

Ideas:

- Facilitar Home Office

## 4. Material Extra

1. [Using data science to understand and attack the coronavirus and other epidemics.](https://towardsdatascience.com/using-data-science-to-understand-and-attack-the-coronavirus-and-other-epidemics-41019c6b0c86)
2. [El tiempo se está acabando](https://medium.com/@ava_3974/el-tiempo-se-est%C3%A1-acabando-37fab8bcde4)
3. [Response to COVID19 in Taiwan](https://jamanetwork.com/journals/jama/fullarticle/2762689?guestAccessKey=2a3c6994-9e10-4a0b-9f32-cc2fb55b61a5&utm_source=For_The_Media&utm_medium=referral&utm_campaign=ftm_links&utm_content=tfl&utm_term=030320)
